package main.java;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;


public class Planta implements Participante {
	public Color color = Color.GREEN;
	public int x=500;
	public int y=1;
	private int dir_x=0;
	private int dir_y=0;
	private int edad = 0;
	private static final int WITH = 15;
	private static final int HEIGH = 15;	
	private static final int VIDA_MAX = 500;	
	private Juego game;
	private boolean muerto = false;
	private int velocidad = 0;
	
	public Planta(Juego game) {
		this.game= game;
		// inicia posición y dirección aleatorias
		this.x = (int)(Math.random()*game.ancho_tablero+1);
		this.y = (int)(Math.random()*game.alto_tablero+1);
	}
	
	public void teclaPresionada(KeyEvent e) {
		/*  No hay control por teclado
		if (e.getKeyCode() == KeyEvent.VK_LEFT)
			dir_x = -1;
		if (e.getKeyCode() == KeyEvent.VK_RIGHT)
			dir_x = 1;
	    */
	}
	
	
	public void recalculaPosicion() {
		// recalcula posición en base a entradas y parametros
		this.x=this.x+dir_x;
		this.y=this.y+dir_y;	
		
		edad = edad + 1;
		if (edad > VIDA_MAX) {
			muerto = true;
		}
	}

	public void repinta(Graphics g) {
		// Pinta el elemento en base a su posici�n, y otros parametros
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setColor(this.color);
		g2d.fillOval(this.x, this.y, WITH, HEIGH);
	}
	
	
	public Participante colision() {
		// indica que el cuadro de o2 intersecta con el mio
		//if (this.game.o2.getBounds().intersects(getBounds())) {
		//	return null;
		//}
		Participante colisionado = null;
		
		for (Participante otro: this.game.participantes) {
			if (otro.getBounds().intersects(getBounds(2))) {
				if (otro != this) {
					colisionado = otro;
				}
			}
		}
		return colisionado;
	}
	
	public Boolean estaMuerto() {
		return muerto;
	}
	
	public void setMuerto() {
		muerto = true;
	}
	
	
	//  POLIMORFISMO:::  Subrecarga de métodos. El mísmo método con diferentes parámetros
	public Rectangle getBounds() {
		return new Rectangle(this.x, this.y, this.WITH, this.HEIGH);
	}
	
	public Rectangle getBounds(int plus) {
		return new Rectangle(this.x - plus, this.y - plus, this.WITH + plus, this.HEIGH + plus);
	}
}
