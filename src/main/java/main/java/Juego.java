package main.java;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Juego  extends JPanel {
		static int ancho_tablero = 600;
		static int alto_tablero = 500;
		public ArrayList<Participante> participantes = new ArrayList<Participante>();

		public Juego() {
			addKeyListener(new KeyListener() {
				@Override
				public void keyTyped(KeyEvent e) {
				}

				@Override
				public void keyReleased(KeyEvent e) {
				}

				@Override
				public void keyPressed(KeyEvent e) {
					
				}
			});
			setFocusable(true);
		}
		
		private void iniciaParticipantes() {
			// Crea conejos
			for (int i = 0; i<15; i++) {
				participantes.add(new Conejo(this));
			}
			for (int i = 0; i<8; i++) {
				participantes.add(new Zorro(this));
			}
			for (int i = 0; i<5; i++) {
				participantes.add(new Planta(this));
			}
		}
		
		private void recalculaPosiciones() {					
			// No puedo utilizar un for (Participante participante:participantes)
			// Porque el array va a modificarse durante el bucle, añadiendo o quitando elementos
			for (int i=0; i< participantes.size(); i++) {
				Participante participante = participantes.get(i);    
			    
			    if (!participante.estaMuerto()) {
			    	participante.recalculaPosicion();
				}
			}
			
			
			Iterator<Participante> iteradorParticipantes = participantes.iterator();
			while(iteradorParticipantes.hasNext()) {     
			    Participante participante = iteradorParticipantes.next();    
			    
			    //System.out.println(participante);
			    if (participante.estaMuerto()) {
			    	iteradorParticipantes.remove();
			    } 
			}
			
			// Nacen una nueva planta cada ciclo
			participantes.add(new Planta(this));
			
			/*
			Participante ult_participante = participantes.get(participantes.size() -1);
			int i = 0;
			Participante participante = participantes.get(0);
			// La diferencia entre el while y el do-while es si se trata o no el elemento que cumple la condición de salida
			do {
				participante.recalculaPosicion();
				i = i+1;
				participante = participantes.get(i);
			} while (participante != ult_participante && i < participantes.size());
			*/
		}
		
		@Override
		public void paint(Graphics g) {		
			super.paint(g);
			
			for (Participante participante:participantes) {
				participante.repinta(g);
			}
		}

		public static void main(String[] args) throws InterruptedException {
			JFrame frame = new JFrame("Titulo");
			Juego game = new Juego();
			frame.add(game);
			frame.setSize(ancho_tablero, alto_tablero);
			frame.setVisible(true);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			game.iniciaParticipantes();
			
			while (true) {
				System.out.println ("Participantes nº: " + game.participantes.size());
				game.recalculaPosiciones();
				game.repaint();
				Thread.sleep(30);
			}
		}
}