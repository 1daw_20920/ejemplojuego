package main.java;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;

public interface Participante {

	public void recalculaPosicion();

	public void repinta(Graphics g);
	
	public Participante colision();
	
	public Rectangle getBounds();
	
	public Rectangle getBounds(int plus);
	
	public Boolean estaMuerto();
	public void setMuerto();
	
}
